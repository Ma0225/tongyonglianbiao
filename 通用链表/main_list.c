#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getch.h>

#include "list.c"

int cmp (const void* ptr1,const void* ptr2)
{	
	const double *p1=ptr1,*p2=ptr2;
	if(*p1>*p2)
		return 1;
	else if(*p1<*p2)
		return -1;
	else return 0;
}


void show(const void* ptr)
{
	const double *p=ptr;
	printf("%lf ",*p);
}


int main()
{
	List* list=create_list();
	double arr[11]={1,2,44,4,2,3,7,4,9,10,10086};
	for(int i=0;i<7;i++)
	{
		add_tail(list,&arr[i]);
	}
		
	show_list(list,show);
	printf("---------------------------\n");
	//add_head(list,&arr[10]);
	//add_index(list,&arr[10],5);
	//show_list(list,show);
	//del_index(list,3);
	//show_list(list,show);
	del_val(list,&arr[6],cmp);
	printf("---------------------------\n");
	show_list(list,show);
}
