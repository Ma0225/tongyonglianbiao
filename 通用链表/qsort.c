#include <stdio.h>
#include <stdlib.h>



int compar(const void* ptr1,const void* ptr2)
{
	const int *p1=ptr1,*p2=ptr2;
	if(*p1>*p2)
		return 1;
	else if(*p1==*p2)
		return 0;
	else return -1;
}

int main()
{
	int arr[5]={3,2,8,9,4};
	qsort(arr,sizeof(arr)/sizeof(arr[0]),sizeof(arr[0]),compar);
}
