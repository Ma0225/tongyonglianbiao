#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
typedef struct Node
{
	void* ptr;///指向数据的首地址
	struct Node* next;//标记元素的下一个位置


}Node;

typedef struct List
{
	Node* head;
	size_t size;

}List;


typedef int (*compar)(const void* ptr1,const void* ptr2);
	

//创建节点
Node* create_node(void* ptr);
//创建链表
List* create_list(void);
//销毁
void destory_list(List* list);
//清空链表
void clear_list(List* list);
//头添加
void  add_head(List* list,void* ptr);
//尾添加
void add_tail(List* list,void* ptr);
//指定位置添加
bool add_index(List* list,void* ptr,int index);
//头删除
bool del_head(List* list);
//尾删除
bool del_tail(List* list);
//指定位置删除
bool del_index(List* list,int index);
//指定值删除
bool del_val(List* list,void* ptr,compar cmp);
//指定位置访问|返回值
void* access_index(List* list,int index);
//指定值访问|返回位置
int access_val(List* list,void* ptr,compar cmp);
//排序
void sort_list(List* list,compar cmp);
//遍历
void show_list(List* list,void(*show)(const void* ptr));

#endif //LIST_H

