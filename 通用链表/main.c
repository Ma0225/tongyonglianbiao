#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getch.h>

#include "list.c"

int cmp (const void* ptr1,const void* ptr2)
{	
	const double *p1=ptr1,*p2=ptr2;
	if(*p1>*p2)
		return 1;
	else if(*p1<*p2)
		return -1;
	else return 0;
}


void show(const void* ptr)
{
	const double *p=ptr;
	printf("%lf ",*p);
}



List* list=NULL;

typedef struct Student
{
	size_t id;
	char name[20];
	float score;
	
}Student;

void show_s(const void* ptr)
{
	const Student* stu=ptr;
	printf("%u %s %f\n",stu->id,stu->name,stu->score);
}
void add_stu(void)
{
	Student* stu=malloc(sizeof(Student));
	printf("请输入学生的学号，姓名，成绩：\n");
	scanf("%u %s %f",&stu->id,stu->name,&stu->score);
	add_tail(list,stu);
}
void del_stu(void)
{
	printf("请选择按姓名删除(n)还是学号删除(i):\n");
	stdin->_IO_read_ptr=stdin->_IO_read_end;
	if('n'==getch())
	{
		char name[20];
		printf("请输入要删除的姓名:");
		gets(name);
		int cmp(const void* ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const char* name=ptr2;
			return strcmp(stu->name,name);
		}
		printf("%s ",del_val(list,name,cmp)?"按姓名删除成功":"按姓名删除失败");
	}else if('i'==getch())
	{
		size_t id=0;
		printf("请输入要删除的学号:");
		scanf("%u",&id);

		int cmp(const void* ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const size_t* id=ptr2;
			if(stu->id>*id)
				return 1;
			else if(stu->id<*id)
				return -1;
			else return 0;
		}
		del_val(list,&id,cmp);
	}
}
void find_stu(void)
{
	printf("请选择按姓名查找(n)还是学号查找(i):\n");
	stdin->_IO_read_ptr=stdin->_IO_read_end;
	if('n'==getch())
	{
		char name[20];
		printf("请输入要查找的姓名:");
		gets(name);
		int cmp(const void* ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const char* name=ptr2;
			return strcmp(stu->name,name);
		}
	int len=access_val(list,&name,cmp);
	Node* node=list->head;
	while(len--)
	{
		node=node->next;
	}
	show_s(node->ptr);
	printf("\n");
	
	}else if('i'==getch())
	{
		printf("请输入要查找的学号:");
		size_t id;
		scanf("%u",&id);
		int cmp(const void* ptr1,const void* ptr2)
		{
			const Student* stu=ptr1;
			const size_t* id=ptr2;
			if(stu->id>*id)
				return 1;
			else if(stu->id<*id)
				return -1;
			else return 0;
		}
		int len=access_val(list,&id,cmp);
		Node* node=list->head;
		while(len--)
		{
			node=node->next;
		}
		show_s(node->ptr);
		printf("\n");
	}
	
}
void modify_stu(void)
{
	printf("请输入需要修改学生的id：\n");
	size_t id;
	scanf("%u",&id);
	int cmp(const void* ptr1,const void* ptr2)
	{
		const Student* stu=ptr1;	
		const size_t* id=ptr2;
		if(stu->id>*id)
			return 1;
		else if(stu->id<*id)
			return -1;
		else return 0;
	}
	int len=access_val(list,&id,cmp);
	if(len==-1)
		printf("未找到该id");
	else 
	{
		Node* node =list->head;
		for(int i=0;i<len;i++)
		{
			node=node->next;
		}
		printf("请输入新的学号，姓名，成绩：\n");
		Student* stu=malloc(sizeof(Student));
		scanf("%u %s %f",&stu->id,stu->name,&stu->score);
		del_index(list,len);
		add_index(list,stu,len);
		
	}
	
}
void show_stu(void)
{
	show_list(list,show_s);
}


int main()
{
	list=create_list();
	add_stu();
	add_stu();
	add_stu();
	//del_stu();
	show_stu();
	//find_stu();
	modify_stu();
	show_stu();
}
/*
int main()
{
	List* list=create_list();
	double arr[11]={1,2,44,4,2,3,7,4,9,10,10086};
	for(int i=0;i<7;i++)
	{
		add_tail(list,&arr[i]);
	}
		
	//add_head(list,&arr[10]);
	add_index(list,&arr[10],5);
	show_list(list,show);
	del_index(list,3);
	show_list(list,show);
	del_val(list,&arr[1],cmp);
	show_list(list,show);
}*/
